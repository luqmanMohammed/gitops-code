#!/bin/sh

pipeline_count=$(curl --header "PRIVATE-TOKEN:${API_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines?scope=running&ref=master" | jq -M  'reduce ( .[] | select(.id != null)) as $i (0;.+=1)')
echo "Currently Running Pipeline Count : ${pipeline_count}"

if [[ $pipeline_count -gt 1 ]]
then
    echo "Already a Pipeline is Running"
    echo "Exiting Current Pipeline. Retry After Completion"
    exit 1
else
    echo "Continuing Pipeline"
    exit 0
fi